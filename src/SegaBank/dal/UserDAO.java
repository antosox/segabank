package src.SegaBank.dal;

import src.SegaBank.bo.User;
import src.SegaBank.dal.PersistenceManager;

import java.io.IOException;
import java.sql.*;
import java.util.List;

public class UserDAO implements IDAO<Integer, User> {

    private static final String SELECT_ID_WHERE_LOGIN_PASSWORD = "SELECT id FROM public.\"user\" WHERE login = ? and password = ?";
    private static final String INSERT_USER_TOTAL = "INSERT INTO public.\"user\" (name, firstname, email, login, password) VALUES (?, ?, ?, ?, ?)";
    private static final String INSERT_USER = "INSERT INTO User VALUES name = ?, firstname = ?, email = ?";
    private static final String SELECT_ALL_USER = "SELECT * FROM User";
    private static final String SELECT_BY_ID = "SELECT name, firstname FROM public.\"user\" WHERE id = ?";
    private static final String SELECT_BY_LOGIN = "SELECT * FROM public.\"user\" WHERE login = ?";

    @Override
    public void create(User user) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(INSERT_USER_TOTAL, Statement.RETURN_GENERATED_KEYS)){
            ps.setString(1, user.getName());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getLogin());
            ps.setString(5, user.getMdp());

            ps.executeUpdate();

            try(ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }
        }
    }

    @Override
    public void update(User object) {

    }

    @Override
    public void remove(User object) {

    }

    @Override
    public boolean findBy(Integer integer) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID)) {
            ps.setInt(1, integer);
            ps.executeQuery();

            try (ResultSet rs = ps.getGeneratedKeys()) {
                return rs.next();
            }
        }
    }

    public User findByLogin(String login) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_LOGIN)) {
            ps.setString(1, login);

            try (ResultSet rs = ps.executeQuery()) {
                //login est unique
                if (rs.next()){
                    User user = new User(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getString("firstname"),
                            rs.getString("login"),
                            rs.getString("password"),
                            rs.getString("email")
                    );
                return user;
                }
            }
        }
    return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    public boolean findUserBy(String login, String password) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(SELECT_ID_WHERE_LOGIN_PASSWORD)) {
            ps.setString(1, login);
            ps.setString(2, password);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()){
                 return true;
                }
            }
        }
        return false;
    }
}
