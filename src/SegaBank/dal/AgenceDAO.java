package src.SegaBank.dal;

import src.SegaBank.bo.Agence;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AgenceDAO implements IDAO<Integer, Agence> {

    private static final String AGENCE = "public.\"agence\"";

    private static final String INSERT_AGENCE = "INSERT INTO "+ AGENCE +" (code, name, adress) VALUES (?,?,?)";
    private static final String SELECT_ALL ="SELECT * FROM " + AGENCE;

    @Override
    public void create(Agence agence) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if(connection != null){
            try(PreparedStatement ps = connection.prepareStatement(INSERT_AGENCE, Statement.RETURN_GENERATED_KEYS)){
                ps.setInt(1, agence.getCode());
                ps.setString(2, agence.getName());
                ps.setString(3, agence.getAdresse());

                ps.executeUpdate();

                try(ResultSet rs = ps.getGeneratedKeys()){
                    if (rs.next()){
                        agence.setId(rs.getInt(1));
                    }
                }
            }

        }
    }

    @Override
    public void update(Agence object) {

    }

    @Override
    public void remove(Agence object) {

    }

    @Override
    public boolean findBy(Integer integer) throws SQLException, IOException, ClassNotFoundException {
        return false;
    }

    @Override
    public List<Agence> findAll() throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        List<Agence> agences = new ArrayList<>();
        if(connection != null){
            try(PreparedStatement ps = connection.prepareStatement(SELECT_ALL)){
                try(ResultSet rs = ps.executeQuery()){
                    while (rs.next()){
                        agences.add(new Agence(
                                rs.getInt("id"),
                                rs.getInt("code"),
                                rs.getString("adress"),
                                rs.getString("name")
                        ));
                    }
                }
            }
        return agences;
        }
        return null;
    }
}
