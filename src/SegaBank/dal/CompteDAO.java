package src.SegaBank.dal;

import src.SegaBank.bo.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompteDAO implements IDAO<Integer, Compte> {

    private static final String COMPTE = "public.\"compte\"";
    private static final String SELECT_BY = "SELECT * FROM "+COMPTE+" WHERE agence = ?";
    private static final String UPDATE_SOLDE = "UPDATE "+COMPTE+" SET solde = ? WHERE id = ?";

    @Override
    public void create(Compte object) throws SQLException, IOException, ClassNotFoundException {

    }

    @Override
    public void update(Compte compte) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if (connection != null){
            try(PreparedStatement ps = connection.prepareStatement(UPDATE_SOLDE)){
                ps.setInt(1, compte.getSolde());
                ps.setInt(2, compte.getIndentifiant());

                ps.executeUpdate();
            }
        }
    }

    @Override
    public void remove(Compte object) {

    }

    @Override
    public boolean findBy(Integer integer){
        return false;
    }

    public void findAgenceBy(Agence agence) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if(connection != null){
            List<Compte> comptes = new ArrayList<>();
            try(PreparedStatement ps = connection.prepareStatement(SELECT_BY)){
                ps.setInt(1, agence.getId());

                try(ResultSet rs = ps.executeQuery()){
                    while (rs.next()){
                        String compteType = rs.getString("type");
                        switch (compteType){
                            case "simple":
                                Compte compte = new CompteSimple(rs.getInt("id"), rs.getInt("solde"), Compte.Type.Simple, rs.getInt("decouvert"));
                                comptes.add(compte);
                                break;
                            case "payant":
                                Compte compte1 = new ComptePayant(rs.getInt("id"), rs.getInt("solde"), Compte.Type.Payant);
                                comptes.add(compte1);
                                break;
                            case "epargne":
                                Compte compte2 = new CompteEpargne(rs.getInt("id"), rs.getInt("solde"), Compte.Type.Epargne, rs.getInt("interest"));
                                comptes.add(compte2);
                                break;
                        }
                    }
                }
            }
            agence.setCompteList(comptes);
        }
    }

    @Override
    public List<Compte> findAll() {
        return null;
    }
}
