package src.SegaBank.dal;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class PersistenceManager {

    private static final String PROPS_FILE = "./resources/db.properties";

    private static Connection connection;

    private PersistenceManager(){}

    public static Connection getConnection() throws IOException, ClassNotFoundException, SQLException {

        if(connection == null || !connection.isValid(2)){
            Properties props = new Properties();
            try(FileInputStream fis = new FileInputStream(PROPS_FILE)){
                props.load(fis);
            }

            //polymorph database
            String driverClass = props.getProperty("jdbc.class.driver");
            //------
            String dburl = props.getProperty("jdbc.db.url");
            String dblogin = props.getProperty("jdbc.db.login");
            String dbmdp = props.getProperty("jdbc.db.mdp");

            Class.forName(driverClass);

            connection = DriverManager.getConnection(dburl,dblogin,dbmdp);
        }
        return connection;
    }

    public static void closeConnection() throws SQLException {
        if (connection != null || connection.isValid(2)){
            connection.close();
        }
    }
}
