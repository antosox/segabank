package src.SegaBank.bo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class CompteEpargne extends Compte {
    private int interet;

    public CompteEpargne(int indentifiant, int solde, Type type, int interet) {
        super(indentifiant, solde, type);
        this.interet=interet;
    }

    public int getInteret() {
        return interet;
    }

    public void setInteret(int interet) {
        this.interet = interet;
    }
    public void calculInteret(){
        solde=solde+(solde*interet/100);
    }

    @Override
    public void versement(int value) throws SQLException, IOException, ClassNotFoundException {
        this.solde=solde+value;
        compteDAO.update(this);
    }

    @Override
    public void retrait(int value) throws SQLException, IOException, ClassNotFoundException {
        this.solde=solde-value;
        compteDAO.update(this);
    }

    @Override
    public String toString() {
        return "CompteEpargne : " + "interet=" + interet + ", indentifiant=" + indentifiant + ", solde=" + solde;
    }
}
