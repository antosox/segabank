package src.SegaBank.bo;

import java.util.List;

public class User {
    public User(int id, String name, String firstname, String login, String mdp, String email) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.login = login;
        this.mdp = mdp;
        this.email = email;
    }

    public User(String name, String firstname, String login, String mdp, String email) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.login = login;
        this.mdp = mdp;
        this.email = email;
    }

    private int id;
    private String name;
    private String firstname;
    private String login;
    private String mdp;
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
