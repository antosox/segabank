package src.SegaBank.bo;

import src.SegaBank.dal.CompteDAO;
import src.SegaBank.dal.IDAO;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Agence {
    private int id;
    private int code;
    private String name;
    private String adresse;
    private List<Compte> compteList;

    //bug si IDAO
    private CompteDAO compteDao = new CompteDAO();

    public Agence(int code, String name, String adresse) {
        this.code = code;
        this.name = name;
        this.adresse = adresse;
    }

    public Agence(int id, int code, String adresse, String name) {
        this.id = id;
        this.code = code;
        this.adresse = adresse;
        this.name = name;
        compteList = new ArrayList<>();

        try {
            compteDao.findAgenceBy(this);
        } catch (SQLException|IOException|ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public List<Compte> getCompteList() {
        return compteList;
    }

    public void setCompteList(List<Compte> compteList) {
        this.compteList = compteList;
    }

    public void addCompte(Compte compte){
        compteList.add(compte);
    }

    public Compte getCompte(Integer integer){
        for (Compte compte:compteList) {
            if (compte.getIndentifiant() == integer){
                return compte;
            }
        }
        return null;
    }
}
