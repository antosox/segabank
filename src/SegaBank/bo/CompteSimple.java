package src.SegaBank.bo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class CompteSimple extends Compte {
    private int decouvert;

    public CompteSimple(int indentifiant, int solde, Type type, int decouvert) {
        super(indentifiant, solde, type);
        this.decouvert=decouvert;
    }

    public int getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(int decouvert) {
        this.decouvert = decouvert;
    }

    @Override
    public void versement(int value) throws SQLException, IOException, ClassNotFoundException {
        this.solde=solde+value;
        compteDAO.update(this);
    }

    @Override
    public void retrait(int value) throws SQLException, IOException, ClassNotFoundException {
        int temp = solde;
        if(solde-value>decouvert){
            this.solde=solde-value;
            compteDAO.update(this);
        }else{
            System.out.println("Vous ne povez pas retirez plus que votre découvert");
        }

    }

    @Override
    public String toString() {
        return "CompteSimple : " + "decouvert=" + decouvert + ", indentifiant=" + indentifiant + ", solde=" + solde;
    }
}
