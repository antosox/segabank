package src.SegaBank.bo;

import src.SegaBank.dal.CompteDAO;

import java.io.IOException;
import java.sql.SQLException;

public abstract class Compte {


    public enum Type {
        Simple,
        Epargne,
        Payant
    }
    protected int indentifiant;
    protected int solde;
    private Type type;

    protected CompteDAO compteDAO = new CompteDAO();

    public Compte(int indentifiant, int solde, Type type) {
        this.indentifiant = indentifiant;
        this.solde = solde;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getIndentifiant() {
        return indentifiant;
    }

    public void setIndentifiant(int indentifiant) {
        this.indentifiant = indentifiant;
    }

    public int getSolde() {
        return solde;
    }

    public void setSolde(int solde) {
        this.solde = solde;
    }

    public abstract void versement(int value) throws SQLException, IOException, ClassNotFoundException;

    public abstract void retrait(int value) throws SQLException, IOException, ClassNotFoundException;

    @Override
    public abstract String toString();
}
