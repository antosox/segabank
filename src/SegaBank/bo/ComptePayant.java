package src.SegaBank.bo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class ComptePayant extends Compte {

    @Override
    public void retrait(int value) throws SQLException, IOException, ClassNotFoundException {
        int frais = value*5/100;
        this.solde=solde-frais;
        this.solde=solde-value;
        compteDAO.update(this);
    }
    @Override
    public void versement(int value) throws SQLException, IOException, ClassNotFoundException {
        int frais = value*5/100;
        this.solde=solde-frais;
        this.solde=solde+value;
        compteDAO.update(this);
    }

    public ComptePayant(int indentifiant, int solde, Type type) {
        super(indentifiant, solde, type);
    }

    @Override
    public String toString() {
        return "ComptePayant : " + "indentifiant=" + indentifiant + ", solde=" + solde;
    }
}
