package src.SegaBank;

import src.SegaBank.bo.*;
import src.SegaBank.dal.AgenceDAO;
import src.SegaBank.dal.CompteDAO;
import src.SegaBank.dal.IDAO;
import src.SegaBank.dal.UserDAO;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class SegaConsole {
    private static Scanner sc = new Scanner(System.in);
    private static IDAO userDao = new UserDAO();
    private static IDAO agenceDao = new AgenceDAO();
    private static IDAO compteDao = new CompteDAO();


    public static void main(String[] args) {

        IDAO userDao = new UserDAO();

        User user = new User("TheHedgehog", "Sonic", "sega", "sega", "sonic@sega.ru");
        try {
            userDao.create(user);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        login();
    }
    public static void login(){
        System.out.println("Bonjour, identifiez-vous !");

        String login, mdp = "";
        boolean first = true;
        boolean state = false;
        do {
            if(!first){
                System.out.println("Erreur l'identifiant et/ou le mot de passe n'est pas correct");
            }
            Scanner sc = new Scanner(System.in);

            System.out.println("login :");
            login = sc.nextLine();
            System.out.println("Mot de Passe :");
            mdp = sc.nextLine();

            try {
                state = ((UserDAO) userDao).findUserBy(login, mdp);
            } catch (SQLException|IOException|ClassNotFoundException e) {
                e.printStackTrace();
            }
            first = false;
            System.out.println(state);
        }while (!state);

        try {
            System.out.println("Bonjour M " + ((UserDAO) userDao).findByLogin(login).getName());
        } catch (SQLException|IOException|ClassNotFoundException e) {
            e.printStackTrace();
        }

        dspAgenceMenu();
    }

    public static void dspAgenceMenu(){
        List<Agence> agences = new ArrayList<>();
        try {
            agences = agenceDao.findAll();
        } catch (SQLException|IOException|ClassNotFoundException e) {
            e.printStackTrace();
        }

        AtomicInteger i = new AtomicInteger(3);
        int reponse;
        boolean first = true;
        do {
            if (!first) {
                System.out.println("Mauvais choix .. merci de recommencer");
            }
            System.out.println("=========================");
            System.out.println("======MENU AGENCE======");
            System.out.println("=========================");
            System.out.println("1 - Ajouter une Agence");
            System.out.println("2 - Retirer une Agence (pas dev)");
            for (Agence agence: agences) {
                System.out.println(i.getAndIncrement()+" - "+agence.getCode() + " " +agence.getName());

            }

            System.out.println("0 - Quitter ");
            System.out.println("Entrez votre choix : ");
            try {
                reponse = sc.nextInt();
            } catch (InputMismatchException e) {
                reponse = -1;
            } finally {
                sc.nextLine();
            }
            first = false;
        } while (reponse < 0 || reponse > i.get());

        switch (reponse) {
            case 1:
                System.out.println("Entrez le code de l'agence :");
                int code = 0;
                try {
                    code = sc.nextInt();
                } catch (InputMismatchException e) {
                    code = -1;
                } finally {
                    sc.nextLine();
                }
                System.out.println("Entrez le nom de l'agence :");
                String nom = "John Doe";
                try {
                    nom = sc.nextLine();
                } catch (InputMismatchException e) {
                }

                System.out.println("Entrez l'email de l'agence :");
                String email = "johndoe@unknow.off";
                try {
                    email = sc.nextLine();
                } catch (InputMismatchException e) {
                }
                Agence newAgence = new Agence(code, nom, email);
                agences.add(newAgence);
                try {
                    agenceDao.create(newAgence);
                } catch (SQLException|IOException|ClassNotFoundException e) {
                    e.printStackTrace();
                }
                dspAgenceMenu();
                break;
            case 2:
                //pas le temps (faut que je respire... mon esprit glisse ailleur)
                dspAgenceMenu();
                break;
            case 0:
                login();
                break;
            default:
                dspMainMenu(agences.get(reponse-3));
        }
    }

    public static void dspMainMenu(Agence agence) {

        List<Compte> comptes = agence.getCompteList();
        int reponse;
        boolean first = true;
        do {
            if (!first) {
                System.out.println("Mauvais choix .. merci de recommencer");
            }
            System.out.println("=========================");
            System.out.println("======MENU COMPTE======");
            System.out.println("=========================");
            System.out.println("1 - Consulter les compte");
            System.out.println("2 - Faire un versement");
            System.out.println("3 - Faire un Retrait");
            System.out.println("4 - Changer d'agence");
            System.out.println("5 - Quitter ");
            System.out.println("Entrez votre choix : ");
            try {
                reponse = sc.nextInt();
            } catch (InputMismatchException e) {
                reponse = -1;
            } finally {
                sc.nextLine();
            }
            first = false;
        } while (reponse < 1 || reponse > 4);

        switch (reponse) {
            case 1:
                for (Compte compte:comptes) {
                    System.out.println(compte.getIndentifiant());
                }
                dspMainMenu(agence);
                break;
            case 2:
                System.out.println("Selectionner l'id du compte :");
                int id;
                try {
                    id = sc.nextInt();
                } catch (InputMismatchException e) {
                    id = -1;
                } finally {
                    sc.nextLine();
                }
                if (agence.getCompte(id) != null){
                    versement(agence.getCompte(id), agence);
                }else {
                    System.out.println("l'id n'existe pas");
                    dspMainMenu(agence);
                }
                break;
            case 3:
                System.out.println("Selectionner l'id du compte :");
                try {
                    id = sc.nextInt();
                } catch (InputMismatchException e) {
                    id = -1;
                } finally {
                    sc.nextLine();
                }
                if (agence.getCompte(id) != null){
                    retrait(agence.getCompte(id), agence);
                }else {
                    System.out.println("l'id n'existe pas");
                    dspMainMenu(agence);
                }
                break;
            case 4:
                dspAgenceMenu();
                break;
            case 5:
                login();
                break;
        }
    }

    public static void versement(Compte compte, Agence agence) {
        int value = 0;

        System.out.println("Quelle est la valeur de votre versement");
        value = sc.nextInt();
        try {
            compte.versement(value);
        } catch (SQLException|IOException|ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Votre compte acutellement : ");
        System.out.println(compte.toString());
        try {
            FileWriter pw = new FileWriter("src\\src\\"+compte.getIndentifiant()+".csv",true);
            pw.append("Versement :" +value);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    dspMainMenu(agence);
    }

    public static void retrait(Compte compte, Agence agence){
        int value = 0;

        System.out.println("Quelle est la valeur de votre retrait");
        value = sc.nextInt();
        try {
            compte.retrait(value);
        }
        catch (SQLException|IOException|ClassNotFoundException e) {
        e.printStackTrace();
        }
        System.out.println("Votre compte acutellement : ");
        System.out.println(compte.toString());
        try {
            FileWriter pw = new FileWriter("src\\src\\"+compte.getIndentifiant()+".csv",true);
            pw.append("Versement :" +value);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        dspMainMenu(agence);
    }
}
